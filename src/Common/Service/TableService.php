<?php


namespace Ox3a\Common\Service;

use Ox3a\Service\DbServiceInterface;
use Zend\Db\TableGateway\TableGateway;


class TableService
{
    const IS_SHARE = true;

    protected $_tables = [];

    /**
     * @var DbServiceInterface
     */
    protected $_dbService;


    /**
     * TableService constructor.
     * @param DbServiceInterface $_dbService
     */
    public function __construct(
        DbServiceInterface $_dbService
    ) {
        $this->_dbService = $_dbService;
    }


    /**
     * @param string $table
     * @return TableGateway
     */
    public function get($table)
    {
        if (empty($this->_tables[$table])) {
            $this->_tables[$table] = new TableGateway($table, $this->_dbService->getAdapter());
        }

        return $this->_tables[$table];
    }

}
