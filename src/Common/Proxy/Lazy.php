<?php

namespace Ox3a\Common\Proxy;

class Lazy
{


    protected $_callback;

    protected $_arguments;


    /**
     * Lazy constructor.
     * @param callable $callback
     * @param array    $arguments
     */
    public function __construct($callback, array $arguments = [])
    {
        $this->_callback  = $callback;
        $this->_arguments = $arguments;
    }


    public function load(array $arguments = [])
    {
        return call_user_func_array($this->_callback, $arguments ?: $this->_arguments);
    }


}
