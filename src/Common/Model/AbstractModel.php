<?php
/**
 * Created by PhpStorm.
 * User: aa
 * Date: 19.11.18
 * Time: 22:38
 */

namespace Ox3a\Common\Model;

use InvalidArgumentException;
use JsonSerializable;

abstract class AbstractModel implements JsonSerializable
{

    const EVENT_POPULATE     = 1;
    const EVENT_SET_PROPERTY = 2;

    /**
     * @var ObserverModel
     */
    protected $_observer;

    protected $_properties = [
        'id' => null,
    ];


    public function __construct($data = null)
    {
        $this->_init();

        $this->_initObserver();

        $this->_initListeners();

        if ($data) {
            $this->populate($data);
        }
    }


    public function getProperty($name)
    {
        if (array_key_exists($name, $this->_properties)) {
            return $this->_properties[$name];
        }

        return null;
    }


    public function __get($name)
    {
        return $this->getProperty($name);
    }


    public function __set($name, $value)
    {
        if (!array_key_exists($name, $this->_properties)) {
            throw new InvalidArgumentException('Неизвестное свойство');
        }

        $this->_properties[$name] = $value;

        $this->_observer->emit(
            self::EVENT_SET_PROPERTY,
            [
                [
                    'class'    => self::class,
                    'event'    => self::EVENT_SET_PROPERTY,
                    'property' => $name,
                ],
            ]
        );
    }


    public function __isset($name)
    {
        return isset($this->_properties[$name]);
    }


    public function populate($data)
    {
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $this->_properties)) {
                $this->_properties[$key] = $value;
            }
        }

        $this->_observer->emit(
            self::EVENT_POPULATE,
            [
                [
                    'class' => self::class,
                    'event' => self::EVENT_POPULATE,
                ],
            ]
        );
    }


    public function toArray()
    {
        return $this->_properties;
    }


    public function jsonSerialize()
    {
        return $this->toArray();
    }


    public function on($event, $listener)
    {
        $this->_observer->on($event, $listener);
        return $this;
    }


    public function off($event, $listener)
    {
        $this->_observer->off($event, $listener);
        return $this;
    }


    public function _init()
    {
    }


    protected function _initObserver()
    {
        $this->_observer = new ObserverModel();
    }


    protected function _initListeners()
    {
    }

}
