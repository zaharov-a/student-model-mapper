<?php
/**
 * Created by PhpStorm.
 * User: aa
 * Date: 03.03.19
 * Time: 13:09
 */

namespace Ox3a\Common\Model;


class ObserverModel
{
    protected $_eventListeners = [];


    /**
     * @param          $event ($event, $args...)
     * @param callable $listener
     */
    public function on($event, $listener)
    {
        if (!isset($this->_eventListeners[$event])) {
            $this->_eventListeners[$event] = [];
        }

        $this->_eventListeners[$event][] = $listener;
    }


    public function off($event, $listener)
    {
        if (!isset($this->_eventListeners[$event])) {
            return;
        }

        $key = array_search($listener, $this->_eventListeners[$event]);
        if ($key) {
            unset($this->_eventListeners[$event][$key]);
        }
    }


    public function emit($event, $data)
    {
        if (!isset($this->_eventListeners[$event])) {
            return;
        }

        foreach ($this->_eventListeners[$event] as $callable) {
            call_user_func_array($callable, $data);
        }
    }
}
