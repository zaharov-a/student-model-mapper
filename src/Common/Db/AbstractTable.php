<?php
/**
 * Created by PhpStorm.
 * User: aa
 * Date: 23.02.19
 * Time: 17:10
 */

namespace Ox3a\Common\Db;


use Ox3a\Service\DbServiceInterface;
use Zend\Db\TableGateway\TableGateway;

class AbstractTable extends TableGateway
{
    /**
     * Флаг того, что это расшариваемый сервис
     * @var bool
     */
    const IS_SHARE = true;

    /**
     * @var DbServiceInterface
     */
    public $db;

    const TABLE_NAME = '';


    public function __construct(DbServiceInterface $db)
    {
        parent::__construct(static::TABLE_NAME, $db->getAdapter());
        $this->db = $db;
    }

}
