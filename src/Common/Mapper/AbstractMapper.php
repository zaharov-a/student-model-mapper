<?php
/**
 * Created by PhpStorm.
 * User: aa
 * Date: 19.11.18
 * Time: 22:45
 */

namespace Ox3a\Common\Mapper;


use Ox3a\Common\Model\AbstractModel;
use Zend\Db\TableGateway\AbstractTableGateway;

abstract class AbstractMapper
{

    /**
     * Флаг того, что это расшариваемый сервис
     * @var bool
     */
    const IS_SHARE = true;

    protected $_map = [];

    /**
     * @var string[]
     */
    protected $_exclude = [];

    protected $_primaryKey = 'id';

    //    protected $_db;
    //
    //    protected $_tableFactory;
    //
    //
    //    public function __construct(Service $db, Table $tableFactory)
    //    {
    //        $this->_db           = $db->get();
    //        $this->_tableFactory = $tableFactory;
    //    }

    abstract public function find($id);


    abstract public function createObject($data = []);


    /**
     * @return AbstractTableGateway
     */
    abstract protected function _getTable();


    public function prepare($data)
    {
        $result = [];

        foreach ($data as $key => $value) {
            if (isset($this->_map[$key])) {
                $result[$this->_map[$key]] = $value;
            }
        }

        return $result;
    }


    public function rePrepare($data)
    {
        $result = [];

        foreach ($this->_map as $field => $property) {
            if (array_key_exists($property, $data)) {
                $result[$field] = $data[$property];
            }
        }

        return array_diff_key($result, array_flip($this->_exclude));
    }


    /**
     * @param AbstractModel $model
     */
    public function save($model)
    {
        $this->_preSave($model);

        $data = $this->_getSaveData($model);

        $primaryProperty = $this->_getPrimaryProperty();

        $table = $this->_getTable();

        if ($model->getProperty($primaryProperty)) {
            $table->update($data, [$this->_primaryKey . '=?' => $model->getProperty($primaryProperty)]);
        } else {
            $table->insert($data);
            $model->populate([$primaryProperty => $table->getLastInsertValue()]);
        }

        $this->_postSave($model);
    }


    /**
     * @param AbstractModel $model
     */
    public function delete($model)
    {
        $primaryProperty = $this->_getPrimaryProperty();
        $this->_getTable()->delete([$this->_primaryKey . '=?' => $model->getProperty($primaryProperty)]);
        $this->_postDelete($model);
    }


    public function checkUniqueFields($fields, $id)
    {
        $data = $this->rePrepare($fields);

        $where    = [];
        $platform = $this->_getTable()->getAdapter()->platform;

        foreach ($data as $field => $value) {
            $where[$platform->quoteIdentifier($field) . '=?'] = $value;
        }

        if ($id) {
            $where[$platform->quoteIdentifier($this->_primaryKey) . '<>?'] = $id;
        }

        $row = $this->_getTable()->select($where)->current();

        return $row ? $row[$this->_primaryKey] : null;
    }


    public function getDbService()
    {
        return $this->_getTable()->getAdapter();
    }


    public function beginTransaction()
    {
        $this->getDbService()->getDriver()->getConnection()->beginTransaction();
    }


    public function commit()
    {
        $this->getDbService()->getDriver()->getConnection()->commit();
    }


    public function rollback()
    {
        $this->getDbService()->getDriver()->getConnection()->rollback();
    }


    protected function _getPrimaryProperty()
    {
        return $this->_map[$this->_primaryKey];
    }


    /**
     * @param AbstractModel $model
     * @return array
     */
    protected function _getSaveData($model)
    {
        $data = $this->rePrepare($model->toArray());

        unset($data[$this->_primaryKey]);

        return $data;
    }


    protected function _preSave($model)
    {
    }


    protected function _postSave($model)
    {
    }


    protected function _postDelete($model)
    {
    }

}
